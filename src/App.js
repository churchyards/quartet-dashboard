import React from 'react';
import styled from 'styled-components';
import './App.css';
import {Scoreboard} from "./components/Scoreboard/Scoreboard";
import {Header} from "./components/Header/Header";
import {Transfers} from "./components/Transfers/Transfers";

const Container = styled.div`
    display: flex;
    flex-direction: column;
`;

const Content = styled.div`
    display: flex;
    flex-direction: row;
`;

function App() {
  return (
    <Container>
      <Header/>
      <Content>
        <Scoreboard />
        <Transfers/>
      </Content>
    </Container>
  );
}

export default App;
