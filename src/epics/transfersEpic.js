import {combineEpics, ofType} from 'redux-observable';
import {from, of, timer} from 'rxjs';
import {catchError, map, switchMap, takeUntil,} from 'rxjs/operators';
import {api} from '../api';
import {
  loadTransfersErrorAction,
  loadTransfersSuccessAction,
  startPollingTransfersAction,
  stopPollingTransfersAction
} from "../reducers/transfersReducer";

export const BASE_URL = '/quartet/api';

export const pollTransfersFromApi = (action$) => {
    const stopPolling$ = action$.pipe(ofType(stopPollingTransfersAction().type));

    return action$.pipe(
        ofType(startPollingTransfersAction().type),
        switchMap(() => {
            return timer(0, 3000).pipe(
                takeUntil(stopPolling$),
                switchMap(() => from(api.get(`${BASE_URL}/transfers`))),
                map(response => toModel(response.data)),
                map(data => data.sort( (a, b) => toDateTime(b.timestamp) - toDateTime(a.timestamp ))),
                map(data => loadTransfersSuccessAction(data)),
                catchError(error => of(loadTransfersErrorAction(error.response))),
            );
        }),
    );
};

const toModel = (data) =>
    data.map( (d) => ({
        ...d,
        time: getTime(d.timestamp)
    }));

const toDateTime = (timestamp) => {
  const year = timestamp[0];
  const month = timestamp[1];
  const day = timestamp[2];
  const hour = timestamp[3];
  const minute = timestamp[4];
  const second = timestamp[5];
  return new Date(year, month, day, hour, minute, second);
};

const getTime = (timestamp) => {
  const hour = timestamp[3];
  const minute = appendLeadingZeros(timestamp[4]);
  const second = appendLeadingZeros(timestamp[5]);
  return `${hour}:${minute}:${second}`;
};

const appendLeadingZeros = (n) => n <= 9 ? `0${n}` : n;

export const transfersEpic = combineEpics(pollTransfersFromApi);
