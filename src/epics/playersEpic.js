import {combineEpics, ofType} from 'redux-observable';
import {from, of, timer} from 'rxjs';
import {catchError, map, switchMap, takeUntil,} from 'rxjs/operators';
import {api} from '../api';
import {
  loadPlayersErrorAction,
  loadPlayersSuccessAction,
  startPollingPlayersAction,
  stopPollingPlayersAction
} from "../reducers/playersReducer";

export const BASE_URL = '/quartet/api';

export const pollPlayersFromApi = (action$) => {
    const stopPolling$ = action$.pipe(ofType(stopPollingPlayersAction().type));

    return action$.pipe(
        ofType(startPollingPlayersAction().type),
        switchMap(() => {
            return timer(0, 5000).pipe(
                takeUntil(stopPolling$),
                switchMap(() => from(api.get(`${BASE_URL}/players`))),
                map(response => toModel(response.data)),
                map(data => data.sort( (a, b) => b.numberOfQuartets - a.numberOfQuartets )),
                map(data => loadPlayersSuccessAction(data)),
                catchError(error => of(loadPlayersErrorAction(error.response))),
            );
        }),
    );
};

const toModel = (data) =>
    data.map( (d) => ({
        ...d,
        numberOfCards: d.status ? d.status.numberOfCards : 0,
        numberOfQuartets: d.status && d.status.quartets ? d.status.quartets.length : 0,
    }));

export const playersEpic = combineEpics(pollPlayersFromApi);
