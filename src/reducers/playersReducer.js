import { createAction, handleActions } from 'redux-actions';

export const LOAD_PLAYERS_SUCCESS = 'LOAD_PLAYERS_SUCCESS';
export const LOAD_PLAYERS_ERROR = 'LOAD_PLAYERS_ERROR';
export const START_POLLING_PLAYERS = 'START_POLLING_PLAYERS';
export const STOP_POLLING_PLAYERS = 'STOP_POLLING_PLAYERS';

export const loadPlayersSuccessAction = createAction(LOAD_PLAYERS_SUCCESS);
export const loadPlayersErrorAction = createAction(LOAD_PLAYERS_ERROR);
export const startPollingPlayersAction = createAction(START_POLLING_PLAYERS);
export const stopPollingPlayersAction = createAction(STOP_POLLING_PLAYERS);

const initialState = {
    players: [],
};

export const playersReducer = handleActions(
    {
        [LOAD_PLAYERS_SUCCESS]: (state, action) => {
            return {
                ...state,
                players: action.payload || [],
            };
        },

        [LOAD_PLAYERS_ERROR]: (state, action) => {
            return {
                ...state,
                players: [],
            };
        },
    },
    initialState,
);
