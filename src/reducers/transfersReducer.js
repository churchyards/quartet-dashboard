import { createAction, handleActions } from 'redux-actions';

export const LOAD_TRANFERS_SUCCESS = 'LOAD_TRANFERS_SUCCESS';
export const LOAD_TRANFERS_ERROR = 'LOAD_TRANFERS_ERROR';
export const START_POLLING_TRANFERS = 'START_POLLING_TRANFERS';
export const STOP_POLLING_TRANFERS = 'STOP_POLLING_TRANFERS';

export const loadTransfersSuccessAction = createAction(LOAD_TRANFERS_SUCCESS);
export const loadTransfersErrorAction = createAction(LOAD_TRANFERS_ERROR);
export const startPollingTransfersAction = createAction(START_POLLING_TRANFERS);
export const stopPollingTransfersAction = createAction(STOP_POLLING_TRANFERS);

const initialState = {
    transfers: [],
};

export const transfersReducer = handleActions(
    {
        [LOAD_TRANFERS_SUCCESS]: (state, action) => {
            return {
                ...state,
                transfers: action.payload || [],
            };
        },

        [LOAD_TRANFERS_ERROR]: (state, action) => {
            return {
                ...state,
                players: [],
            };
        },
    },
    initialState,
);
