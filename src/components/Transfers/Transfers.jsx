import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';
import {startPollingTransfersAction, stopPollingTransfersAction} from "../../reducers/transfersReducer";

const Container = styled.div`
    width: 35%;
    height: 1000px;
    background-color: snow;
`;

const StyledTable = styled.div`
    margin: 30px;
    width: 89%;
    font-size: 16px;
`;

export class TransfersComponent extends React.PureComponent {

  componentWillMount() {
    this.props.startPollingTransfers();
  }

  componentWillUnmount() {
    this.props.stopPollingTransfers();
  }

  render() {
    return (
      <Container>
        <StyledTable>
          <tr>
            <th>Card</th>
            <th>From</th>
            <th>To</th>
            <th>Time</th>
          </tr>
          {
            this.props.transfers.transfers.map( t => (
              <tr>
                <td>{t.cardName}</td>
                <td>{t.from}</td>
                <td>{t.to}</td>
                <td>{t.time}</td>
              </tr>
            ))
          }
        </StyledTable>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  transfers: state.transfers
});

const mapDispatchToProps = dispatch => ({
  startPollingTransfers: () => {
    dispatch(startPollingTransfersAction());
  },
  stopPollingTransfers: () => {
    dispatch(stopPollingTransfersAction());
  },
});

export const Transfers = connect(
  mapStateToProps,
  mapDispatchToProps,
)(TransfersComponent);



