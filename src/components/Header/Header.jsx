import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    width: 100%;
    height: 80px;
    background-color: tomato;
    display: flex;
    flex-direction: row;
    flex-wrap: nowrap;
    justify-content: left;
`;

const Title = styled.span`
  color: white;
  font-size: 28px;
  font-weight: bold;
  margin: 22px;
`;

export const Header = () => {

    return (
      <Container>
        <Title>Micronaut Quartet</Title>
      </Container>
    )
};


