import React from 'react';
import styled from 'styled-components';

const Container = styled.div`
    font-size: 12px;
`;

export const Quartets = (props) => {
    return (
      <Container>
        <ul>
          {
            props.items ? props.items.map( name => (
              <li>{name}</li>
            )) : null
          }
        </ul>
      </Container>
    )
};



