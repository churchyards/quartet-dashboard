import React from 'react';
import {connect} from 'react-redux';
import styled from 'styled-components';

import {startPollingPlayersAction, stopPollingPlayersAction} from "../../reducers/playersReducer";
import {Quartets} from "../Quartets/Quartets";

const Container = styled.div`
    width: 65%;
`;

const StyledTable = styled.table`
    margin: 30px;
    width: 93%;
`;

export class ScoreboardComponent extends React.PureComponent {

  componentWillMount() {
    this.props.startPollingPlayers();
  }

  componentWillUnmount() {
    this.props.stopPollingPlayers();
  }

  render() {
    return (
      <Container>
        <StyledTable>
          <tr>
            <th>Player</th>
            <th>IP address</th>
            <th>Cards</th>
            <th colSpan="2">Quartets</th>
          </tr>
          {
            this.props.players.players.map( p => (
              <tr>
                <td>{p.name}</td>
                <td>{p.ipAddress}</td>
                <td>{p.numberOfCards}</td>
                <td>{p.numberOfQuartets}</td>
                <td><Quartets items={p.status.quartets} /></td>
              </tr>
            ))
          }
        </StyledTable>
      </Container>
    )
  }
}

const mapStateToProps = state => ({
  players: state.players
});

const mapDispatchToProps = dispatch => ({
  startPollingPlayers: () => {
    dispatch(startPollingPlayersAction());
  },
  stopPollingPlayers: () => {
    dispatch(stopPollingPlayersAction());
  },
});

export const Scoreboard = connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScoreboardComponent);


