import {applyMiddleware, combineReducers, createStore} from 'redux';
import {composeWithDevTools} from 'redux-devtools-extension';
import {combineEpics, createEpicMiddleware} from 'redux-observable';
import {playersEpic} from "./epics/playersEpic";
import {playersReducer} from "./reducers/playersReducer";
import {transfersReducer} from "./reducers/transfersReducer";
import {transfersEpic} from "./epics/transfersEpic";

const rootReducer = combineReducers({
  players: playersReducer,
  transfers: transfersReducer
});

const epicMiddleware = createEpicMiddleware();

export const store = createStore(rootReducer, composeWithDevTools(applyMiddleware(epicMiddleware)));

const allEpics = combineEpics(playersEpic, transfersEpic);

epicMiddleware.run(allEpics);
